# Local Overlay Tool
## Written by Dirk Colbry 9/22/23
#


This tool is designed to mimic the overlay powertools previously written.  I am working to make improvements. The basic usage is as follows:

### Step 1: Build an overlay 

```overlay --create```

### Step 2: Activate an overlay instance 

```overlay --activate```

### Step 3: Install software using conda

```conda install jupyter```

### Step 4: Check the installation is inside the overlay

```which jupyter```

### Step 5: Deactivate the overlay

```exit```

Start the overlay again by skipping steps 1 and 3 and going strait to step 2.

